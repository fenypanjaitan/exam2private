import React, { Component } from 'react';
import Header from './Header';

export class FormLogin extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            username:'',
            password:''
         }
    }

    handleChange = (evt) => {
		this.setState({ [evt.target.name]: evt.target.value });
		console.log(evt.target.value);
    };
    
    handleSubmit = (evt) => {
        alert( this.state.username + " " + this.state.password);
        console.log(this.state.nama);
        evt.preventDefault();
    }
	render() {
		return (
			<div>
				<Header />
				<br />
				<form onSubmit={this.handleSubmit} className="container card">
                <h3> Login Penjaga </h3>
				<div className="row section">
					<label>Username</label>
					<input
						name="username"
						type="text"
						placeholder="Username"
						value={this.state.username}
						onChange={this.handleChange}
					/>
				</div>
				<div className="row section">
					<label>Password</label>
					<input
						name="password"
						type="password"
						placeholder="Password"
						value={this.state.password}
						onChange={this.handleChange}
					/>
				</div>
				<div className="btn">
					<button onClick={this.props.handlerLogin}>Login</button>
				</div>
                </form>
			</div>
		);
	}
}

export default FormLogin;
