import React, { Component } from 'react';
import Header from '../Header';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

export class CrudBuku extends Component {
    constructor(props) {
		super(props);
		this.state = {
			title: 'CRUD Buku',
			act: 0,
			index: '',
			bukus: []
		};
    } 
    
    componentDidMount() {
		this.names.judul.focus();
    }

    onSubmit=(e)=>{
        e.preventDefault();
        let bukus = this.state.bukus;
		let judul = this.name.judul.value;
        let tterbit = this.name.tterbit.value;
        let id_penulis = this.name.id_penulis.value;
        let id_penerbit = this.name.id_penerbit.value;
        
        if (this.state.act === 0) {
			//new
			let buku = {
				judul,
                tterbit,
                id_penulis,
                id_penerbit
			};
			bukus.push(buku);
		} else {
			//update
			let index = this.state.index;
			bukus[index].judul = judul;
            bukus[index].tterbit = tterbit;
            bukus[index].id_penulis = id_penulis;
			bukus[index].id_penerbit = id_penerbit;
		}

		this.setState({
			bukus: bukus,
			act: 0
        });
        this.name.myForm.reset();
		this.names.judul.focus();
    }

    fRemove = (i) => {
		let bukus = this.state.bukus;
		bukus.splice(i, 1);
		this.setState({
			bukus: bukus
		});

		this.names.myForm.reset();
		this.names.name.focus();
	};

	

	render() {
		return (
			<div>
				<Header />
				<br />
				<br />
				<Container>
					<form  name="myForm" className="container card">
						<h3> {this.state.title} </h3>
						<div className="row section">
							<label>Judul</label>
							<input name="judul" type="text" placeholder="judul" />
						</div>
						<div className="row section">
							<label>Tanggal terbit</label>
							<input name="tterbit" type="text" placeholder="tanggal terbit" />
						</div>
						<div className="row section">
							<label>id_penulis</label>
							<input name="id_penulis" type="text" placeholder="id_penulis" />
						</div>
						<div className="row section">
							<label>id_penerbit</label>
							<input name="id_penerbit" type="text" placeholder="id_penerbit" />
						</div>
						<div className="btn">
							<button onClick={(e) => this.onSubmit(e)}>Submit {''}</button>
						</div>
					</form>
				</Container>

                <pre>
						{this.state.bukus.map((buku, i) => (
							<li key={i} className="myList">
								{i + 1}. {buku.name}, {buku.address}
								<button onClick={() => this.fRemove(i)} className="myListButton">
									remove{' '}
								</button>
								<button onClick={() => this.fEdit(i)} className="myListButton">
									edit{' '}
								</button>
							</li>
						))}
					</pre>
			</div>
		);
	}
}

export default CrudBuku;
