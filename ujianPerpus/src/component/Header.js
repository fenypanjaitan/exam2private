import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container, Navbar,  } from 'react-bootstrap'

export class Header extends Component {
    render() {
        return (
            <div>
                <Container>
                    <Navbar variant="dark" bg="dark">
                        <Navbar.Brand href="#">Perpustakaan</Navbar.Brand>
                    </Navbar>
                </Container>
            </div>
        )
    }
}

export default Header