import React from 'react';
import './App.css';
import FormLogin from './component/FormLogin';
import Switch from 'react-bootstrap/esm/Switch';
import { Route } from 'react-router-dom';
import CrudBuku from './component/buku/CrudBuku';

function App() {
	return (
		<Switch>
			<Route path="/" exact component={FormLogin} />
			<Route path="/crudBuku" component={CrudBuku} />
		</Switch>
	);
}

export default App;
